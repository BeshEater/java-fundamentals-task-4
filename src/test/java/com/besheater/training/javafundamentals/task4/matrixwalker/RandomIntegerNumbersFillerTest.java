package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RandomIntegerNumbersFillerTest {

    @Test
    public void testThatFillsWithIntegers() {
        double[][] data = { { 1.58, 2.41, 3.47 },
                            { 5.99, 5.55, 5.12 },
                            { 9.84, 7.74, 1.38 } };
        RealMatrix matrix = makeMatrix(data);
        matrix.walkInOptimizedOrder(new RandomIntegerNumbersFiller(0, 100));

        for (int rowIndex = 0; rowIndex < matrix.getRowDimension(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < matrix.getColumnDimension(); columnIndex++) {
                double value = matrix.getEntry(rowIndex, columnIndex);
                assertEquals(value, Math.floor(value));
            }
        }
    }

    @Test
    public void testThatFillsWithinRange() {
        RealMatrix matrix = new Array2DRowRealMatrix(1000, 1000);
        matrix.walkInOptimizedOrder(new RandomIntegerNumbersFiller(-20, -10));

        for (int rowIndex = 0; rowIndex < matrix.getRowDimension(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < matrix.getColumnDimension(); columnIndex++) {
                double value = matrix.getEntry(rowIndex, columnIndex);
                assertTrue(value >= -20 && value < -10);
            }
        }
    }

    private RealMatrix makeMatrix(double[][] data) {
        return new Array2DRowRealMatrix(data);
    }
}