package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.DefaultRealMatrixPreservingVisitor;

public class MaxEntryFinder extends DefaultRealMatrixPreservingVisitor {
    double maxValue = -Double.MAX_VALUE;

    @Override
    public void visit(int row, int column, double value) {
        if (value > maxValue) {
            maxValue = value;
        }
    }

    @Override
    public double end() {
        return maxValue;
    }
}