package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.DefaultRealMatrixPreservingVisitor;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RowsWithoutValueFinder extends DefaultRealMatrixPreservingVisitor {
    private final double value;
    private final Set<Integer> rowsWithValue;
    private int rowsCount;

    public RowsWithoutValueFinder(double value) {
        this.value = value;
        this.rowsWithValue = new HashSet<>();
    }

    @Override
    public void start(int rows, int columns, int startRow, int endRow,
                      int startColumn, int endColumn) {
        this.rowsCount = rows;
    }

    @Override
    public void visit(int row, int column, double value) {
        if (this.value == value) {
            rowsWithValue.add(row);
        }
    }

    public int[] getRows() {
        Set<Integer> rowsWithoutValue = getAllRows();
        rowsWithoutValue.removeAll(rowsWithValue);
        return rowsWithoutValue.stream()
                               .mapToInt(Number::intValue)
                               .toArray();
    }

    private Set<Integer> getAllRows() {
        return IntStream.range(0, rowsCount)
                        .boxed()
                        .collect(Collectors.toCollection(TreeSet::new));
    }
}