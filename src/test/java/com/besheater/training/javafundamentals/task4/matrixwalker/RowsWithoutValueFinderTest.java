package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RowsWithoutValueFinderTest {

    @Test
    public void testOnSampleMatrix1() {
        double[][] data = { { 1, 2, 3 },
                            { 0, 1, 5 },
                            { 1, 7, 1 } };
        double value = 1;
        int[] rowsWithoutValue = {};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix2() {
        double[][] data = { { 4, 2, 3 },
                            { 0, 1, 5 },
                            { 9, 7, 1 } };
        double value = 1;
        int[] rowsWithoutValue = {0};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix3() {
        double[][] data = { {-17,  -3,  -2 },
                            { -9, -87, -13 },
                            {-47,  -8, -98 } };
        double value = -2;
        int[] rowsWithoutValue = {1, 2};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix4() {
        double[][] data = { { -158,  584, -478,  789 },
                            {    4, -748, -387,   42 },
                            {  -98,  888,    0, 9478 },
                            { -789, -987, 7489, 8749 } };
        double value = 888;
        int[] rowsWithoutValue = {0, 1, 3};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix5() {
        double[][] data = { { -158,  584, -478,  789 },
                            {    4, -748, -387,   42 },
                            {  -98,  888,    1, 9478 },
                            { -789, -987, 7489, 8749 } };
        double value = 0;
        int[] rowsWithoutValue = {0, 1, 2, 3};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix6() {
        double[][] data = { { -158,  -99, -478,  748 },
                            {    4, -748, -387,   42 },
                            {  -99,  888,    1,  -77 },
                            { -789, -987,  -99, 8749 } };
        double value = -99;
        int[] rowsWithoutValue = {1};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix7() {
        double[][] data = { { -158,  584, -478 },
                            {    4, -748,  -99 },
                            {   11, -748, -387 },
                            {  888,  -99,    0 },
                            {  741,  111,   -1 },
                            { -789, -987, 7489 } };
        double value = -99;
        int[] rowsWithoutValue = {0, 2, 4, 5};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix8() {
        double[][] data = { { 0, -1 },
                            { 1,  0 } };
        double value = 0;
        int[] rowsWithoutValue = {};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix9() {
        double[][] data = { { 0, -1 },
                            { 1,  0 } };
        double value = 478;
        int[] rowsWithoutValue = {0, 1};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix10() {
        double[][] data = { { 47 } };
        double value = 1;
        int[] rowsWithoutValue = {0};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    @Test
    public void testOnSampleMatrix11() {
        double[][] data = { { -8 } };
        double value = -8;
        int[] rowsWithoutValue = {};
        RowsWithoutValueFinder finder = new RowsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(rowsWithoutValue, finder.getRows());
    }

    private RealMatrix makeMatrix(double[][] data) {
        return new Array2DRowRealMatrix(data);
    }
}