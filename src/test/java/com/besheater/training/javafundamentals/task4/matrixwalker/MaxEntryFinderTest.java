package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxEntryFinderTest {

    @Test
    public void testOnSampleMatrix1() {
        double[][] data = { { 1, 2, 3 },
                            { 5, 5, 5 },
                            { 9, 7, 1 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(9, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    @Test
    public void testOnSampleMatrix2() {
        double[][] data = { {-10, 52,  91 },
                            { 44, 99, -99 },
                            { 88, 98,   0 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(99, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    @Test
    public void testOnSampleMatrix3() {
        double[][] data = { {-17,  -3,  -9 },
                            { -2, -87, -13 },
                            {-47,  -8, -98 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(-2, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    @Test
    public void testOnSampleMatrix4() {
        double[][] data = { { -158,  584, -478,  253 },
                            {    4, -748, -387,   42 },
                            {  888,  -98,    0, 9478 },
                            { -789, -987, 7489, 8749 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(9478, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    @Test
    public void testOnSampleMatrix5() {
        double[][] data = { { -158,  584, -478 },
                            {    4, -748, -387 },
                            {   11, -748, -387 },
                            {  888,  -98,    0 },
                            {  741,  111,   -1 },
                            { -789, -987, 7489 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(7489, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    @Test
    public void testOnSampleMatrix6() {
        double[][] data = { { 0, -1 },
                            { 1,  0 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(1, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    @Test
    public void testOnSampleMatrix7() {
        double[][] data = { { 7, 7 },
                            { 7, 7 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(7, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    @Test
    public void testOnSampleMatrix8() {
        double[][] data = { { 47 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(47, matrix.walkInOptimizedOrder(new MaxEntryFinder()));
    }

    private RealMatrix makeMatrix(double[][] data) {
        return new Array2DRowRealMatrix(data);
    }
}