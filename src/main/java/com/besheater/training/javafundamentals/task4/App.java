package com.besheater.training.javafundamentals.task4;

import com.besheater.training.javafundamentals.task4.matrixwalker.ColumnsWithoutValueFinder;
import com.besheater.training.javafundamentals.task4.matrixwalker.MaxEntryFinder;
import com.besheater.training.javafundamentals.task4.matrixwalker.RandomIntegerNumbersFiller;
import com.besheater.training.javafundamentals.task4.matrixwalker.RowsWithoutValueFinder;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.IntStream;

public class App {
    public static final int MATRIX_MIN_DIMENSION = 1;
    public static final int MATRIX_MAX_DIMENSION = 1_000; // Increase cautiously
    public static final int ENTRY_MAX_VALUE = 1_000_000_000; // Just looks nice
    public static final int ENTRY_MIN_VALUE = 1;

    public static void main( String[] args ) {
        startApp(new Scanner(System.in), System.out);
    }

    public static void startApp(Scanner scanner, PrintStream out) {
        int dimension;
        int maxEntry;
        int referenceColumnIndex;
        try {
            dimension = getMatrixDimension(scanner, out);
            maxEntry = getMaxEntry(scanner, out);
            referenceColumnIndex = getReferenceColumnIndex(scanner, out, dimension);
        } catch (IllegalArgumentException ex) {
            out.println(ex.getMessage());
            return;
        }

        RealMatrix originalMatrix = MatrixUtils.getSquareMatrix(dimension);
        fillMatrixWithRandomIntegers(originalMatrix, maxEntry);
        RealMatrix sortedMatrix = getSortedMatrix(originalMatrix, referenceColumnIndex);
        Optional<RealMatrix> croppedMatrix =
                getCroppedMatrix(sortedMatrix, getMaxEntry(sortedMatrix));

        out.println("Original matrix:");
        out.println(MatrixUtils.prettyPrint(originalMatrix));
        out.println("Sorted matrix:");
        out.println(MatrixUtils.prettyPrint(sortedMatrix));
        out.println("Entries summation:");
        out.println(getEntriesSummation(sortedMatrix));
        out.printf("Matrix max entry = %.0f\n", getMaxEntry(sortedMatrix));
        out.println("Matrix with rows and columns which contained max entry removed:");
        if (croppedMatrix.isPresent()) {
            out.println(MatrixUtils.prettyPrint(croppedMatrix.get()));
        } else {
            out.println("[]\n");
        }

        out.println("That's all. Bye!");
    }

    public static int getMatrixDimension(Scanner scanner, PrintStream out) {
        out.printf("Please enter square matrix dimension (N) from %d to %d:\n",
                   MATRIX_MIN_DIMENSION, MATRIX_MAX_DIMENSION);
        String dimensionStr = scanner.nextLine();
        try {
            return parseMatrixDimension(dimensionStr);
        } catch (IllegalArgumentException ex) {
            String message = String.format("Your dimension is not valid. " +
                            "Please provide integer value from %d to %d next time. Bye!",
                            MATRIX_MIN_DIMENSION, MATRIX_MAX_DIMENSION);
            throw new IllegalArgumentException(message, ex);
        }
    }

    public static int getMaxEntry(Scanner scanner, PrintStream out) {
        out.printf("Please enter maximum value for matrix entries (M) from %d to %d:\n",
                    ENTRY_MIN_VALUE, ENTRY_MAX_VALUE);
        String maxValueStr = scanner.nextLine();
        try {
            return parseMaxValue(maxValueStr);
        } catch (IllegalArgumentException ex) {
            String message = String.format("Your maximum value is not valid. " +
                    "Please provide integer value from %d to %d next time. Bye!",
                    ENTRY_MIN_VALUE, ENTRY_MAX_VALUE);
            throw new IllegalArgumentException(message, ex);
        }
    }

    public static int getReferenceColumnIndex(Scanner scanner, PrintStream out,
                                              int maxColumnNum) {
        out.println("Please enter column number (k). " +
                    "According to that column rows in matrix will be sorted:");
        String columnStr = scanner.nextLine();
        try {
            return parseColumn(columnStr, maxColumnNum);
        } catch (IllegalArgumentException ex) {
            String message = "Your column number is not valid. " +
                    "Please provide integer value from 1 " +
                    "to your matrix dimension (N) next time. Bye!";
            throw new IllegalArgumentException(message, ex);
        }
    }

    public static int parseMatrixDimension(String dimensionStr) {
        int dimension = Integer.parseInt(dimensionStr);
        if (dimension < MATRIX_MIN_DIMENSION || dimension > MATRIX_MAX_DIMENSION) {
            String message = String.format("Matrix dimension is outside of %d to %d range",
                                            MATRIX_MIN_DIMENSION, MATRIX_MAX_DIMENSION);
            throw new IllegalArgumentException(message);
        }
        return dimension;
    }

    public static int parseMaxValue(String maxValueStr) {
        int maxValue = Integer.parseInt(maxValueStr);
        if (maxValue < ENTRY_MIN_VALUE || maxValue > ENTRY_MAX_VALUE) {
            String message = String.format("Matrix max value is outside of %d to %d range",
                                            ENTRY_MIN_VALUE, ENTRY_MAX_VALUE);
            throw new IllegalArgumentException(message);
        }
        return maxValue;
    }

    public static int parseColumn(String columnStr, int maxColumnNum) {
        int columnNum = Integer.parseInt(columnStr);
        if (columnNum < 1 || columnNum > maxColumnNum) {
            String message = "Column number is outside of the range";
            throw new IllegalArgumentException(message);
        }
        return columnNum - 1; // in code columns zero based
    }

    public static void fillMatrixWithRandomIntegers(RealMatrix matrix, int maxValue) {
        int minValue = -1 * maxValue;
        maxValue += 1; // must add one to include value itself
        matrix.walkInOptimizedOrder(new RandomIntegerNumbersFiller(minValue, maxValue));
    }

    public static RealMatrix getSortedMatrix(RealMatrix originalMatrix, int column) {
        if (column < 0 || column >= originalMatrix.getColumnDimension()) {
            throw new IllegalArgumentException("Column number is outside of the matrix range");
        }
        double[][] data = originalMatrix.getData();
        Arrays.sort(data, Comparator.comparingDouble( row -> row[column] ));
        return new Array2DRowRealMatrix(data);
    }

    public static String getEntriesSummation(RealMatrix matrix) {
        StringBuilder builder = new StringBuilder();
        for (int rowIndex = 0; rowIndex < matrix.getRowDimension(); rowIndex++) {
            double[] row = matrix.getRow(rowIndex);
            OptionalInt firstPositiveValueIndex = getFirstPositiveValueIndex(row);
            OptionalInt secondPositiveValueIndex = getSecondPositiveValueIndex(row);

            if (firstPositiveValueIndex.isPresent() && secondPositiveValueIndex.isPresent()) {
                double firstPositiveValue = row[firstPositiveValueIndex.getAsInt()];
                double secondPositiveValue = row[secondPositiveValueIndex.getAsInt()];
                double entriesSum = getEntriesSum(row, firstPositiveValueIndex.getAsInt(),
                                                          secondPositiveValueIndex.getAsInt());
                builder.append(String.format("%d. Elements: %.0f, %.0f; Sum: %.0f;\n",
                        rowIndex + 1, firstPositiveValue, secondPositiveValue, entriesSum));
            } else {
                builder.append(String.format("%d. Doesn't have 2 positive numbers;\n",
                        rowIndex + 1));
            }
        }
        return builder.toString();
    }

    private static double getEntriesSum(double[] arr, int from, int to) {
        double[] entriesInsideRange = Arrays.copyOfRange(arr, from + 1, to);
        return Arrays.stream(entriesInsideRange).sum();
    }

    private static OptionalInt getFirstPositiveValueIndex(double[] arr) {
        return IntStream.range(0, arr.length).filter( i -> arr[i] > 0)
                                             .findFirst();
    }

    private static OptionalInt getSecondPositiveValueIndex(double[] arr) {
        return IntStream.range(0, arr.length).filter( i -> arr[i] > 0)
                                             .skip(1)
                                             .findFirst();
    }

    private static double getMaxEntry(RealMatrix matrix) {
        return matrix.walkInOptimizedOrder(new MaxEntryFinder());
    }

    public static Optional<RealMatrix> getCroppedMatrix(RealMatrix matrix, double value) {
        int[] selectedRows = getRowsWithoutValue(matrix, value);
        int[] selectedColumns = getColumnsWithoutValue(matrix, value);
        if (selectedRows.length == 0 || selectedColumns.length == 0) {
            return Optional.empty();
        }
        double[][] croppedData = new double[selectedRows.length][selectedColumns.length];
        matrix.copySubMatrix(selectedRows, selectedColumns, croppedData);
        return Optional.of(new Array2DRowRealMatrix(croppedData));
    }

    private static int[] getRowsWithoutValue(RealMatrix matrix, double value) {
        RowsWithoutValueFinder rowsWithoutValueFinder = new RowsWithoutValueFinder(value);
        matrix.walkInOptimizedOrder(rowsWithoutValueFinder);
        return rowsWithoutValueFinder.getRows();
    }

    private static int[] getColumnsWithoutValue(RealMatrix matrix, double value) {
        ColumnsWithoutValueFinder columnsWithoutValueFinder = new ColumnsWithoutValueFinder(value);
        matrix.walkInOptimizedOrder(columnsWithoutValueFinder);
        return columnsWithoutValueFinder.getColumns();
    }
}