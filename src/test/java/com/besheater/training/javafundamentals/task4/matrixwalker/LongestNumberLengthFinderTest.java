package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestNumberLengthFinderTest {

    @Test
    public void testOnSampleMatrix1() {
        double[][] data = { { 1, 2, 3 },
                            { 5, 0, 5 },
                            { 9, 7, 1 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(1, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    @Test
    public void testOnSampleMatrix2() {
        double[][] data = { {  1,  2,  -3 },
                            { -5,  0,  55 },
                            {  9, -7,   1 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(2, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    @Test
    public void testOnSampleMatrix3() {
        double[][] data = { {-17,   -3,  -9 },
                            { -2, -870, -13 },
                            {-47,   -8, -98 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(3, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    @Test
    public void testOnSampleMatrix4() {
        double[][] data = { { -17748, -3147,  -9 },
                            {  -2418, -8707, -13 },
                            {     47,  7778, -98 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(5, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    @Test
    public void testOnSampleMatrix5() {
        double[][] data = { { -158,  584, -478 },
                            {    4, -748, -387 },
                            {   11, -748, -387 },
                            {  888,  -98,    0 },
                            { -789, -987, 7489 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(4, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    @Test
    public void testOnSampleMatrix6() {
        double[][] data = { { -158,  584, -478,  253 },
                {    4, -748, -387,   42 },
                {  888,  -98,    0, 9478 },
                { -789, -987, 7489, 8749 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(4, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    @Test
    public void testOnSampleMatrix7() {
        double[][] data = { { 7, 7 },
                            { 7, 7 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(1, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    @Test
    public void testOnSampleMatrix8() {
        double[][] data = { { 123456789 } };
        RealMatrix matrix = makeMatrix(data);
        assertEquals(9, matrix.walkInOptimizedOrder(new LongestNumberLengthFinder()));
    }

    private RealMatrix makeMatrix(double[][] data) {
        return new Array2DRowRealMatrix(data);
    }
}