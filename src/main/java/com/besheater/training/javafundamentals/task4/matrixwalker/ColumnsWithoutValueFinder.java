package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.DefaultRealMatrixPreservingVisitor;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ColumnsWithoutValueFinder extends DefaultRealMatrixPreservingVisitor {
    private final double value;
    private final Set<Integer> columnsWithValue;
    private int columnsCount;

    public ColumnsWithoutValueFinder(double value) {
        this.value = value;
        this.columnsWithValue = new HashSet<>();
    }

    @Override
    public void start(int rows, int columns, int startRow, int endRow,
                      int startColumn, int endColumn) {
        this.columnsCount = columns;
    }

    @Override
    public void visit(int row, int column, double value) {
        if (this.value == value) {
            columnsWithValue.add(column);
        }
    }

    public int[] getColumns() {
        Set<Integer> columnsWithoutValue = getAllColumns();
        columnsWithoutValue.removeAll(columnsWithValue);
        return columnsWithoutValue.stream()
                                  .mapToInt(Number::intValue)
                                  .toArray();
    }

    private Set<Integer> getAllColumns() {
        return IntStream.range(0, columnsCount)
                        .boxed()
                        .collect(Collectors.toCollection(TreeSet::new));
    }
}