package com.besheater.training.javafundamentals.task4;

import com.besheater.training.javafundamentals.task4.matrixwalker.LongestNumberLengthFinder;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

public class MatrixUtils {

    public static String prettyPrint(RealMatrix matrix) {
        int entryWidth = getLongestNumberLength(matrix) + 1;
        String rowPrefix = "[ ";
        String rowSuffix = " ]";
        String rowSeparator = "\n";
        String columnSeparator = " | ";
        String entryFormatPattern = "%" + entryWidth + ".0f";
        StringBuilder builder = new StringBuilder();
        for (int rowIndex = 0; rowIndex < matrix.getRowDimension(); rowIndex++) {
            builder.append(rowPrefix);
            for (int columnIndex = 0; columnIndex < matrix.getColumnDimension(); columnIndex++) {
                double entry = matrix.getEntry(rowIndex, columnIndex);
                builder.append(String.format(entryFormatPattern, entry));
                if (columnIndex != matrix.getColumnDimension() - 1) {
                    builder.append(columnSeparator);
                }
            }
            builder.append(rowSuffix);
            builder.append(rowSeparator);
        }
        return builder.toString();
    }

    public static RealMatrix getSquareMatrix(int dimension) {
        return new Array2DRowRealMatrix(dimension, dimension);
    }

    public static int getLongestNumberLength(RealMatrix matrix) {
        return (int) matrix.walkInOptimizedOrder(new LongestNumberLengthFinder());
    }
}