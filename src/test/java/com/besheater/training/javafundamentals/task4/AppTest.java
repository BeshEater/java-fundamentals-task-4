package com.besheater.training.javafundamentals.task4;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class AppTest {

    @Test
    public void parseMatrixDimension_nullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseMatrixDimension(null));
    }

    @Test
    public void parseMatrixDimension_invalidInput_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseMatrixDimension(""));
        assertThrows(IllegalArgumentException.class, () -> App.parseMatrixDimension(" "));
        assertThrows(IllegalArgumentException.class, () -> App.parseMatrixDimension("sdx"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMatrixDimension("0"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMatrixDimension("-1"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMatrixDimension("-9848"));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseMatrixDimension(String.valueOf(App.MATRIX_MIN_DIMENSION - 1)));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseMatrixDimension(String.valueOf(App.MATRIX_MAX_DIMENSION + 1)));
    }

    @Test
    public void parseMatrixDimension_validInput_returnsWhatExpected() {
        assertEquals(1, App.parseMatrixDimension("1"));
        assertEquals(1, App.parseMatrixDimension("01"));
        assertEquals(5, App.parseMatrixDimension("5"));
        assertEquals(10, App.parseMatrixDimension("0010"));
        assertEquals(100, App.parseMatrixDimension("100"));
        assertEquals(849, App.parseMatrixDimension("849"));
        assertEquals(App.MATRIX_MIN_DIMENSION,
                App.parseMatrixDimension(String.valueOf(App.MATRIX_MIN_DIMENSION)));
        assertEquals(App.MATRIX_MIN_DIMENSION,
                App.parseMatrixDimension(String.valueOf(App.MATRIX_MIN_DIMENSION)));
    }

    @Test
    public void parseMaxValue_nullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseMaxValue(null));
    }

    @Test
    public void parseMaxValue_invalidInput_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseMaxValue(""));
        assertThrows(IllegalArgumentException.class, () -> App.parseMaxValue(" "));
        assertThrows(IllegalArgumentException.class, () -> App.parseMaxValue("sdx"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMaxValue("0"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMaxValue("-1"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMaxValue("-9848"));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseMaxValue(String.valueOf(App.ENTRY_MIN_VALUE - 1)));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseMaxValue(String.valueOf(App.ENTRY_MAX_VALUE + 1)));
    }

    @Test
    public void parseMaxValue_validInput_returnsWhatExpected() {
        assertEquals(1, App.parseMaxValue("1"));
        assertEquals(1, App.parseMaxValue("01"));
        assertEquals(5, App.parseMaxValue("5"));
        assertEquals(10, App.parseMaxValue("0010"));
        assertEquals(100, App.parseMaxValue("100"));
        assertEquals(849, App.parseMaxValue("849"));
        assertEquals(1000000, App.parseMaxValue("1000000"));
        assertEquals(App.ENTRY_MIN_VALUE,
                App.parseMaxValue(String.valueOf(App.ENTRY_MIN_VALUE)));
        assertEquals(App.ENTRY_MAX_VALUE,
                App.parseMaxValue(String.valueOf(App.ENTRY_MAX_VALUE)));
    }

    @Test
    public void parseColumn_nullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn(null, 10));
    }

    @Test
    public void parseColumn_invalidInput_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn("", 10));
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn(" ", 10));
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn("sdx", 10));
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn("0", 10));
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn("-1", 10));
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn("8", 4));
        assertThrows(IllegalArgumentException.class, () -> App.parseColumn("11", 10));
    }

    @Test
    public void parseColumn_validInput_returnsWhatExpected() {
        assertEquals(0, App.parseColumn("1", 10));
        assertEquals(1, App.parseColumn("2", 10));
        assertEquals(2, App.parseColumn("3", 10));
        assertEquals(0, App.parseColumn("01", 10));
        assertEquals(4, App.parseColumn("5", 10));
        assertEquals(9, App.parseColumn("0010", 10));
        assertEquals(99, App.parseColumn("100", 200));
        assertEquals(848, App.parseColumn("849", 1000));
    }

    @Test
    public void fillMatrixWithRandomIntegers_nullArguments_throwsException() {
        assertThrows(NullPointerException.class,
                () -> App.fillMatrixWithRandomIntegers(null, 10));
    }

    @Test
    public void fillMatrixWithRandomIntegers_cleanMatrix_fillsWithinInterval() {
        RealMatrix matrix = new Array2DRowRealMatrix(1000, 10000);
        App.fillMatrixWithRandomIntegers(matrix, 10);

        for (int rowIndex = 0; rowIndex < matrix.getRowDimension(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < matrix.getColumnDimension(); columnIndex++) {
                double value = matrix.getEntry(rowIndex, columnIndex);
                assertTrue(value >= -10 && value <= 10);
            }
        }
    }

    @Test
    public void fillMatrixWithRandomIntegers_sampleMatrix_fillsWithIntegers() {
        double[][] data = { { 1.58, 2.41, 3.47 },
                            { 5.99, 5.55, 5.12 },
                            { 9.84, 7.74, 1.38 } };
        RealMatrix matrix = makeMatrix(data);
        App.fillMatrixWithRandomIntegers(matrix, 1000000);

        for (int rowIndex = 0; rowIndex < matrix.getRowDimension(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < matrix.getColumnDimension(); columnIndex++) {
                double value = matrix.getEntry(rowIndex, columnIndex);
                assertEquals(value, Math.floor(value));
            }
        }
    }

    @Test
    public void getSortedMatrix_nullArguments_throwsException() {
        assertThrows(NullPointerException.class,
                () -> App.getSortedMatrix(null, 0));
    }

    @Test
    public void getSortedMatrix_columnOutsideOfMatrixRange_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> App.getSortedMatrix(new Array2DRowRealMatrix(2, 4), 4));
        assertThrows(IllegalArgumentException.class,
                () -> App.getSortedMatrix(new Array2DRowRealMatrix(10, 10), 500));
        assertThrows(IllegalArgumentException.class,
                () -> App.getSortedMatrix(new Array2DRowRealMatrix(10, 5), -1));
    }

    @Test
    public void getSortedMatrix_sampleMatrix1_sortsAsExpected() {
        double[][] originalData = { { 9, 2, 3 },
                                    { 5, 0, 5 },
                                    { 1, 7, 1 } };

        double[][] sortedData = { { 1, 7, 1 },
                                  { 5, 0, 5 },
                                  { 9, 2, 3 } };
        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 0);
        assertArrayEquals(sortedData, matrix.getData());
    }

    @Test
    public void getSortedMatrix_sampleMatrix2_sortsAsExpected() {
        double[][] originalData = { {-17,   -3,  -9 },
                                    { -2, -870, -13 },
                                    {-47,   -8, -98 } };

        double[][] sortedData = { {-47,   -8, -98 },
                                  { -2, -870, -13 },
                                  {-17,   -3,  -9 } };
        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 2);
        assertArrayEquals(sortedData, matrix.getData());
    }

    @Test
    public void getSortedMatrix_sampleMatrix3_sortsAsExpected() {
        double[][] originalData = { { -158,  584, -478,  253 },
                                    {    4, -748, -387,   42 },
                                    {  888,  98,     0, 9478 },
                                    { -789, -987, 7489, 8749 } };

        double[][] sortedData = { { -789, -987, 7489, 8749 },
                                  {    4, -748, -387,   42 },
                                  {  888,  98,     0, 9478 },
                                  { -158,  584, -478,  253 } };
        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 1);
        assertArrayEquals(sortedData, matrix.getData());
    }

    @Test
    public void getSortedMatrix_sampleMatrix4_sortsAsExpected() {
        double[][] originalData = { { -158,  584,  -10,  253 },
                                    {    4, -748, -387,   42 },
                                    {  888,  98,   -10, 9478 },
                                    { -789, -987,  500, 8749 } };

        double[][] sortedData = { {    4, -748, -387,   42 },
                                  { -158,  584,  -10,  253 },
                                  {  888,  98,   -10, 9478 },
                                  { -789, -987,  500, 8749 } };
        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 2);
        assertArrayEquals(sortedData, matrix.getData());
    }

    @Test
    public void getSortedMatrix_sampleMatrix5_sortsAsExpected() {
        double[][] originalData = { { 17,  584,  -10,  253, 478 },
                                    { 17, -748, -387,   42, 100 },
                                    { 17, -987,  500, 8749, -73 } };

        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 0);
        assertArrayEquals(originalData, matrix.getData());
    }

    @Test
    public void getSortedMatrix_sampleMatrix6_sortsAsExpected() {
        double[][] originalData = { { -158,  584,  -10,   42 },
                                    {    4, -748, -387,  253 },
                                    {  888,  98,   -10, 8478 },
                                    { -789, -987,  500, 9749 } };

        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 3);
        assertArrayEquals(originalData, matrix.getData());
    }

    @Test
    public void getSortedMatrix_sampleMatrix7_sortsAsExpected() {
        double[][] originalData = { { 2, 7 },
                                    { 1, 7 } };

        double[][] sortedData = { { 1, 7 },
                                  { 2, 7 } };
        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 0);
        assertArrayEquals(sortedData, matrix.getData());
    }

    @Test
    public void getSortedMatrix_sampleMatrix8_sortsAsExpected() {
        double[][] originalData = { { 123456789 } };
        RealMatrix matrix = App.getSortedMatrix(makeMatrix(originalData), 0);
        assertArrayEquals(originalData, matrix.getData());
    }

    @Test
    public void printEntriesSum_nullArguments_throwsException() {
        assertThrows(NullPointerException.class, () -> App.getEntriesSummation(null));
    }

    @Test
    public void printEntriesSum_sampleMatrix1_printsWhatExpected() {
        double[][] data = { {  17,  584,  -10,  253 },
                            {  17, -740, -260,   42 },
                            {  -9,  98,   -10, -198 },
                            { -41, -987,  500, 8749 } };
        String expectedOutput = "1. Elements: 17, 584; Sum: 0;\n" +
                                "2. Elements: 17, 42; Sum: -1000;\n" +
                                "3. Doesn't have 2 positive numbers;\n" +
                                "4. Elements: 500, 8749; Sum: 0;\n";
        assertEquals(expectedOutput, App.getEntriesSummation(makeMatrix(data)));
    }

    @Test
    public void printEntriesSum_sampleMatrix2_printsWhatExpected() {
        double[][] data = { { -1, -2, -3 },
                            {  0, -1, -5 },
                            { -1,  7, -1 } };
        String expectedOutput = "1. Doesn't have 2 positive numbers;\n" +
                                "2. Doesn't have 2 positive numbers;\n" +
                                "3. Doesn't have 2 positive numbers;\n";
        assertEquals(expectedOutput, App.getEntriesSummation(makeMatrix(data)));
    }

    @Test
    public void printEntriesSum_sampleMatrix3_printsWhatExpected() {
        double[][] data = { { -17,  584,  -10,  253, 478 },
                            {   1,    0, -387,   42, 100 },
                            {  17, -987,  500, 8749, -73 } };
        String expectedOutput = "1. Elements: 584, 253; Sum: -10;\n" +
                                "2. Elements: 1, 42; Sum: -387;\n" +
                                "3. Elements: 17, 500; Sum: -987;\n";
        assertEquals(expectedOutput, App.getEntriesSummation(makeMatrix(data)));
    }

    @Test
    public void printEntriesSum_sampleMatrix4_printsWhatExpected() {
        double[][] data = { {  0, -584,  -10,    0,   0 },
                            { -8,    0, -387,   42, 100 },
                            { 13,   -1, -500, -100,   1 } };
        String expectedOutput = "1. Doesn't have 2 positive numbers;\n" +
                                "2. Elements: 42, 100; Sum: 0;\n" +
                                "3. Elements: 13, 1; Sum: -601;\n";
        assertEquals(expectedOutput, App.getEntriesSummation(makeMatrix(data)));
    }

    @Test
    public void printEntriesSum_sampleMatrix5_printsWhatExpected() {
        double[][] data = { { 0, -1 },
                            { 1,  7 } };
        String expectedOutput = "1. Doesn't have 2 positive numbers;\n" +
                                "2. Elements: 1, 7; Sum: 0;\n";
        assertEquals(expectedOutput, App.getEntriesSummation(makeMatrix(data)));
    }

    @Test
    public void printEntriesSum_sampleMatrix6_printsWhatExpected() {
        double[][] data = { { 47 } };
        String expectedOutput = "1. Doesn't have 2 positive numbers;\n";
        assertEquals(expectedOutput, App.getEntriesSummation(makeMatrix(data)));
    }

    @Test
    public void getCroppedMatrix_nullArguments_throwsException() {
        assertThrows(NullPointerException.class, () -> App.getCroppedMatrix(null, 1));
    }

    @Test
    public void getCroppedMatrix_sampleMatrix1_returnsWhatExpected() {
        double[][] originalData = { { 9, 2, 3 },
                                    { 5, 0, 5 },
                                    { 1, 7, 8 } };

        double[][] croppedData = { { 9, 2 },
                                   { 5, 0 }};
        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), 8);
        assertArrayEquals(croppedData, matrix.get().getData());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix2_returnsWhatExpected() {
        double[][] originalData = { { 9, 2, 3 },
                                    { 5, 0, 5 },
                                    { 1, 7, 8 } };

        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), 10);
        assertArrayEquals(originalData, matrix.get().getData());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix3_returnsWhatExpected() {
        double[][] originalData = { { -158,  584,  -10,   42 },
                                    {    4, -748, -387,  253 },
                                    {  717,  98,   -10, 8478 },
                                    { -789, -987,  500,  717 } };

        double[][] croppedData = { {  584,  -10 },
                                   { -748, -387 } };
        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), 717);
        assertArrayEquals(croppedData, matrix.get().getData());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix4_returnsWhatExpected() {
        double[][] originalData = { { -17,  584,  -10,  253, 478 },
                                    {   1,    0, -387,   42, 100 },
                                    {  17, -987,  500, 8749, -73 } };

        double[][] croppedData = { { -17,  584,  253, 478 },
                                   {  17, -987, 8749, -73 } };
        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), -387);
        assertArrayEquals(croppedData, matrix.get().getData());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix5_returnsWhatExpected() {
        double[][] originalData = { { -17,  584,  -10,  253, 478 },
                                    {   1,    0, -387,   42, 100 },
                                    {  17, -987,  500, 8749, -73 },
                                    {   0, 1987,  -74, 8749,   0 } };

        double[][] croppedData = { { -10,  253 },
                                   { 500, 8749 } };
        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), 0);
        assertArrayEquals(croppedData, matrix.get().getData());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix6_returnsWhatExpected() {
        double[][] originalData = { { -158,  584, -478 },
                                    {    4, -748, -387 },
                                    { -158, -748, -387 },
                                    {  888,  -98,    0 },
                                    { -158, -987, 7489 } };

        double[][] croppedData = { { -748, -387 },
                                   {  -98,    0 } };

        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), -158);
        assertArrayEquals(croppedData, matrix.get().getData());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix7_returnsWhatExpected() {
        double[][] originalData = { { -17,  584,  -10,  253,   0 },
                                    {   1,    0, -387,   42, 100 },
                                    {  17, -987,  500, 8749,   0 },
                                    {   0, 1987,  -74, 8749,   0 } };

        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), 0);
        assertFalse(matrix.isPresent());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix8_returnsWhatExpected() {
        double[][] originalData = { { 0, -1 },
                                    { 1,  0 } };

        double[][] croppedData = { { 1 } };

        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), -1);
        assertArrayEquals(croppedData, matrix.get().getData());
    }

    @Test
    public void getCroppedMatrix_sampleMatrix9_returnsWhatExpected() {
        double[][] originalData = { { -41 } };

        Optional<RealMatrix> matrix = App.getCroppedMatrix(makeMatrix(originalData), -41);
        assertFalse(matrix.isPresent());
    }

    private RealMatrix makeMatrix(double[][] data) {
        return new Array2DRowRealMatrix(data);
    }
}