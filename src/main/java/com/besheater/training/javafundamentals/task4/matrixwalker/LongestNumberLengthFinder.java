package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.DefaultRealMatrixPreservingVisitor;

/**
 * <p>Returns longest number length in matrix.</p>
 * <p>All entries in matrix will be casted into long for length calculation</p>
 * <p>Length defined as digits count in a decimal representation of entry value</p>
 */
public class LongestNumberLengthFinder extends DefaultRealMatrixPreservingVisitor {
    private long longestLength = Long.MIN_VALUE;

    @Override
    public void visit(int row, int column, double value) {
        long currentLength = getNumberLength((long) value);
        if (currentLength > longestLength) {
            longestLength = currentLength;
        }
    }

    @Override
    public double end() {
        return longestLength;
    }

    public long getNumberLength(long num) {
        return Long.valueOf(Math.abs(num)).toString().length();
    }
}