package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.DefaultRealMatrixChangingVisitor;

import java.util.concurrent.ThreadLocalRandom;

public class RandomIntegerNumbersFiller extends DefaultRealMatrixChangingVisitor {
    private final int origin;
    private final int bound;

    /**
     * <p>Creates {@link RandomIntegerNumbersFiller} that will fill matrix with random integer values
     * between the specified origin (inclusive) and the specified bound (exclusive)</p>
     * @param origin the least possible entry (inclusive)
     * @param bound the max possible entry (exclusive)
     */
    public RandomIntegerNumbersFiller(int origin, int bound) {
        this.origin = origin;
        this.bound = bound;
    }

    @Override
    public double visit(int row, int column, double value) {
        return ThreadLocalRandom.current().nextInt(origin, bound);
    }
}