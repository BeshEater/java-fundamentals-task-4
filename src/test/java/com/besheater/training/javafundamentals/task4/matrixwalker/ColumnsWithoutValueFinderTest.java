package com.besheater.training.javafundamentals.task4.matrixwalker;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ColumnsWithoutValueFinderTest {

    @Test
    public void testOnSampleMatrix1() {
        double[][] data = { { 1, 2, 3 },
                            { 0, 1, 5 },
                            { 1, 7, 1 } };
        double value = 1;
        int[] columnsWithoutValue = {};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix2() {
        double[][] data = { { 4, 2, 3 },
                            { 0, 1, 5 },
                            { 9, 7, 1 } };
        double value = 1;
        int[] columnsWithoutValue = {0};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix3() {
        double[][] data = { {-17,  -3,  -9 },
                            { -2, -87, -13 },
                            {-47,  -8, -98 } };
        double value = -2;
        int[] columnsWithoutValue = {1, 2};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix4() {
        double[][] data = { { -158,  584, -478,  789 },
                            {    4, -748, -387,   42 },
                            {  -98,  888,    0, 9478 },
                            { -789, -987, 7489, 8749 } };
        double value = 888;
        int[] columnsWithoutValue = {0, 2, 3};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix5() {
        double[][] data = { { -158,  584, -478,  789 },
                            {    4, -748, -387,   42 },
                            {  -98,  888,    1, 9478 },
                            { -789, -987, 7489, 8749 } };
        double value = 0;
        int[] columnsWithoutValue = {0, 1, 2, 3};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix6() {
        double[][] data = { { -158,  -99, -478,  748 },
                            {    4, -748, -387,   42 },
                            {  -99,  888,    1,  -77 },
                            { -789, -987,  -99, 8749 } };
        double value = -99;
        int[] columnsWithoutValue = {3};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix7() {
        double[][] data = { { -158,  -99, -478,  748 },
                            {    4, -748, -387,   42 } };
        double value = -387;
        int[] columnsWithoutValue = {0, 1, 3};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix8() {
        double[][] data = { { 0, -1 },
                            { 1,  0 } };
        double value = 0;
        int[] columnsWithoutValue = {};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix9() {
        double[][] data = { { 0, -1 },
                            { 1,  0 } };
        double value = 478;
        int[] columnsWithoutValue = {0, 1};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix10() {
        double[][] data = { { 47 } };
        double value = 1;
        int[] columnsWithoutValue = {0};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    @Test
    public void testOnSampleMatrix11() {
        double[][] data = { { -8 } };
        double value = -8;
        int[] columnsWithoutValue = {};
        ColumnsWithoutValueFinder finder = new ColumnsWithoutValueFinder(value);
        makeMatrix(data).walkInOptimizedOrder(finder);
        assertArrayEquals(columnsWithoutValue, finder.getColumns());
    }

    private RealMatrix makeMatrix(double[][] data) {
        return new Array2DRowRealMatrix(data);
    }
}